import { Component } from '@stencil/core';
import { Navbar } from '../functional';

@Component({
    tag: 'page-about',
    styleUrl: 'page-about.scss'
})
export class About {
    render() {
        return <Navbar title="About" />;
    }
}

