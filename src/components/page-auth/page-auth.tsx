import { Component, State } from '@stencil/core';
import { Navbar } from '../functional';
import { authSvc } from '../../services/auth.service';

@Component({
    tag: 'page-auth',
    styleUrl: 'page-auth.scss'
})
export class PageAuth {

    @State() email: string;

    async sendLink() {
        try {
            if (this.email) {
                await authSvc.sendEmailLink(this.email.trim());
                console.log('Email Sent');
            }
        } catch (error) {
            console.log(error.code, error.message);
        }
    }

    private inputHandler(event) {
        this.email = event.target.value;
    }

    render() {
        return [
            <Navbar title="Authentication" />,
            <ion-content>
                <ion-grid>
                    <ion-row>
                        <ion-col sizeMd="6" offsetMd="3" sizeLg="3" offsetLg="5">
                            <ion-item>
                                <ion-label position='floating'>Email</ion-label>
                                <ion-input required type="email" onInput={event => this.inputHandler(event)} />
                            </ion-item>
                            <ion-button onClick={() => this.sendLink()} margin-top expand="block">Send Link</ion-button>
                            <ion-button onClick={() => authSvc.google()} expand="block">Login with Google</ion-button>
                        </ion-col>
                    </ion-row>
                </ion-grid>
            </ion-content>
        ];
    }
}

