import firebase from 'firebase/app';
// import { Plugins } from '@capacitor/core';

import { auth } from '../firebase';
// const { Modals } = Plugins;

export class AuthService {
    // user: firebase.User;

    public testMethod() {
        console.log('test');
    }

    public sendEmailLink(email: string) {
        const actionCodeSettings = {
            url: 'http://localhost:3333/',
            handleCodeInApp: true
        };
        localStorage.setItem("emailForSignIn", email);
        console.log(actionCodeSettings);
        // return auth.sendSignInLinkToEmail(email, actionCodeSettings);
    }

    // public async verifyEmailLink(url: string) {
    //     if (auth.isSignInWithEmailLink(url)) {
    //         let email = localStorage.getItem('emailForSignIn');
    //         if (!email) {
    //             email = await this.openPrompt();
    //         }
    //         const result = await auth.signInWithEmailLink(email, url);
    //         console.log("FirebaseUser: ", result);

    //         if (result.additionalUserInfo.isNewUser) {
    //             // do something for new users
    //         }

    //         if (history && history.replaceState) {
    //             history.replaceState({}, document.title, url.split('?')[0]);
    //         }

    //         localStorage.removeItem('emailForSignIn');
    //     }
    // }

    public google() {
        const provider = new firebase.auth.GoogleAuthProvider();
        return this.providerHandler(provider);
    }

    // private async openPrompt() {
    //     const prompt = await Modals.prompt({
    //         title: "Email Verification",
    //         message: "Please provide your email for confirmation",
    //         inputPlaceholder: "Email address"
    //     });
    //     return prompt.value;
    // }

    private providerHandler(provider: any) {
        console.log(provider);
        return auth.signInWithPopup(provider);
    }
}

export const authSvc = new AuthService();

