import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
import 'firebase/storage';

export const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDvnZLTVPb41yVl1tJhMri153KdwEa3P0A",
    authDomain: "firesten-app-b625e.firebaseapp.com",
    databaseURL: "https://firesten-app-b625e.firebaseio.com",
    projectId: "firesten-app-b625e",
    storageBucket: "firesten-app-b625e.appspot.com",
    messagingSenderId: "686693957125"
});

export const auth = firebaseApp.auth();
export const firestore = firebaseApp.firestore();
export const storage = firebaseApp.storage();
export const functions = firebaseApp.functions('europe-west1');

